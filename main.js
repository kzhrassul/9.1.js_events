document.addEventListener('DOMContentLoaded', () => {
   
    const section = document.getElementById("app-section--join");
    
    // Create Header => Join our rpogram
    const header = document.createElement("h2");
    header.classList.add("app-section--join-app-title");
    const headerText = document.createTextNode("Join Our Program");
    header.appendChild(headerText);
    
    section.appendChild(header);


    // Create paragraph
    const joinSecParagraph = document.createElement("p");
    joinSecParagraph.classList.add("app-section--join-app-article");
    const joinSecParagraphContent = document.createTextNode("Sed do eiusmod tempro incididunt ut labore et dolore magna aliqua");
    joinSecParagraph.appendChild(joinSecParagraphContent);
    section.appendChild(joinSecParagraph);
    

    // Create a div element
    const div = document.createElement("div");
    div.classList.add("app-section", "app-section--div");
    section.appendChild(div);
    
    //  Create Input text field
    const joinInput = document.createElement("input");
    joinInput.classList.add("app-section--join-input-field");
    joinInput.type = "email";
    joinInput.placeholder = "Email";
    div.appendChild(joinInput);


    //  Create Button
    const joinButton = document.createElement("button");
    joinButton.className = "app-section--subscribe-button";
    joinButtonText = document.createTextNode("SUBSCRIBE");
    joinButton.appendChild(joinButtonText);
    div.appendChild(joinButton);

    // button Onclick event
    joinButton.addEventListener('click', e => {
        e.preventDefault();
        if (joinInput.value === "") {
            alert('Email is incoorect')
        } else {
            alert(joinInput.value)
        }
    })
    
 });




/* <section class="app-section app-section--join">
    <h2 class="app-section--join-app-title">Join Our Program</h2>
    <p class="app-section--join-app-article">Sed do eiusmod tempro incididunt <br> 
          ut labore et dolore magna aliqua</p>
    <div class="app-section app-section--div">
        <input class="app-section--join-input-field" type="Email" value="Email">
        <button class="app-section--subscribe-button">SUBSCRIBE</button>
    </div>
</section> */